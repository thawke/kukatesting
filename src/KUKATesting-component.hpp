#ifndef OROCOS_KUKATESTING_COMPONENT_HPP
#define OROCOS_KUKATESTING_COMPONENT_HPP

#include <rtt/RTT.hpp>

#include <lwr_fri/typekit/Types.hpp>
#include <geometry_msgs/typekit/Types.hpp>
#include <rtt/os/TimeService.hpp>
#include <motion_control_msgs/typekit/Types.hpp>

using namespace RTT;

class kukaTesting : public RTT::TaskContext{

	//Input Port
	InputPort<lwr_fri::FriJointState> port_joint_state_;

	//Output Ports
	//OutputPort<lwr_fri::FriJointImpedance> port_fri_joint_impedance_;
	//OutputPort<motion_control_msgs::JointEfforts> port_joint_effort_command_;
	OutputPort<motion_control_msgs::JointPositions> port_joint_pos_command_;

	//Set up member vars
	lwr_fri::FriJointState joint_state_;
	//lwr_fri::FriJointImpedance fri_joint_impedance_;
	//motion_control_msgs::JointEfforts joint_effort_cmd_;
	motion_control_msgs::JointPositions joint_pos_cmd_;

	float alpha_;
	float omega_curr_;
	float omega_des_;
	float theta_curr_;
	float theta_des_;
	int admit_joint_;
	float I_link_;

	//Orocos Properties
	float t_out_;

	//Other
	RTT::os::TimeService::ticks t_start_;
	RTT::os::TimeService::Seconds t_cur_;
	RTT::os::TimeService::Seconds t_last_;
	RTT::os::TimeService::Seconds t_disp_;

	public:
	kukaTesting(std::string const& name);
	bool configureHook();
	bool startHook();
	void updateHook();
	void stopHook();
	void cleanupHook();
};
#endif
