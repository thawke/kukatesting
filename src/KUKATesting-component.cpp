#include "KUKATesting-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

kukaTesting::kukaTesting(std::string const& name) : TaskContext(name), t_out_(1), t_disp_(0), t_last_(0){
	//Setup data ports
	this->addEventPort("FriJointState", port_joint_state_);
	// event port - updatehook will be called when port_joint_state_ is updated

	//this->addPort("FriJointImpedance", port_fri_joint_impedance_);
	//this->addPort("JointEffortCommand", port_joint_effort_command_);
	this->addPort("JointPositionCommand", port_joint_pos_command_);

	//Setup properties
	this->addProperty("T_out",t_out_);
}

bool kukaTesting::configureHook() {

	//joint_effort_cmd_.efforts.resize(7);
	joint_pos_cmd_.positions.resize(7);

	//Set Joint Impedances and initialise outputs	
	//for(int i=0; i<7; i++) {
	//fri_joint_impedance_.stiffness[i] = 500;
	//fri_joint_impedance_.damping[i] = 0.5;
	//joint_effort_cmd_.efforts[i] = 0;
	//}

	//Time setup
	t_start_ = RTT::os::TimeService::Instance()->getTicks();

	admit_joint_ = 5;	// set the joint for admittance control (=n-1)
	return true;
}

bool kukaTesting::startHook(){
	while(port_joint_state_.read(joint_state_)!=NewData) {
		std::cout << "Waiting for data from KUKA" << std::endl;
		usleep(1e6);
	}
	//Set joint setpoints to current position
	port_joint_state_.read(joint_state_);

	for(int i=0; i<7; i++) {
		joint_pos_cmd_.positions[i] = joint_state_.msrJntPos[i];
	}

	port_joint_pos_command_.write(joint_pos_cmd_);		
	
	omega_curr_ = 0;	// set initial velocity
	I_link_ = 1;		// set inertia of link (kg m^2)
	return true;
}

static float dt = 0.005;

void kukaTesting::updateHook(){
	//Read data
	t_cur_ = RTT::os::TimeService::Instance()->getSeconds(t_start_);
	//float dt = t_cur_ - t_last_;
	//t_last_ = t_cur_;
	port_joint_state_.read(joint_state_);
	theta_curr_ = joint_state_.msrJntPos[admit_joint_]; 
	
	alpha_ = joint_state_.estExtJntTrq[admit_joint_]/I_link_;
	omega_des_ = omega_curr_ + (alpha_ * dt);
	theta_des_ = theta_curr_ + (omega_curr_ * dt) + (0.5 * alpha_ * dt * dt);


	//Display data
	if (t_cur_ - t_disp_ > t_out_) {
		std::cout << "Measured joint torque 5: " << joint_state_.estExtJntTrq[admit_joint_] << " Nm" << std::endl;
		std::cout << "Time step: " << dt << std::endl;
		std::cout << "Current position 5: " << theta_curr_ << std::endl;
		std::cout << "Desired position 5: " << theta_des_ << std::endl << std::endl; 
		t_disp_ = t_cur_;
	}

	omega_curr_ = omega_des_;
	
	joint_pos_cmd_.positions[admit_joint_] = theta_des_;
	port_joint_pos_command_.write(joint_pos_cmd_);

}

void kukaTesting::stopHook() {
}

void kukaTesting::cleanupHook() {
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(kukaTesting)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(kukaTesting)
